const users = [
	{id: 1, nick: 'sky', play: false},
	{id: 2, nick: 'sky2', play: true},
	{id: 3, nick: 'sky3', play: false}
];

module.exports = io => {
	io.on('connection', socket => {
			socket.emit('usersonline', users); // emite y se crea una funcioon para escucharlo en main.js del frontend
			console.log('Nuevo usuario conectado con ID: ' + socket.id);
	});
};