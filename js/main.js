import router from './routes.js';

const state = new Vue({
	data: {
		myUser: null,
		users: [],
		player: null,
		entry: false
	}
});

Vue.component('navbar', {
	template: '#navbar'
});

Vue.component('socketListener', {
	sockets: {
		usersonline(users) {
			state.users = users;
		}
	}
});

const socket = new VueSocketIO({
	debug: true,
	connection: 'http://localhost:3000'
});

Vue.use(socket);

new Vue({
	el: '#app',
	router
});