const Home = { name: 'home', template: '#home-template' }
const Play = { name: 'play', template: '#play-template' }

const routes = [
		{ path: '/', name: 'home', component: Home },
		{ path: '/play', props: true, name: 'play', component: Play }
	];

const router = new VueRouter({ mode: 'history', base: 'triqui', routes });

export default router;
